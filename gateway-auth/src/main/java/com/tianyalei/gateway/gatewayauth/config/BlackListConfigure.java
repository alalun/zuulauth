package com.tianyalei.gateway.gatewayauth.config;

import com.tianyalei.gateway.gatewayauth.gateway.filter.BlackListFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.gateway.config.GatewayAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liyunfeng 2020-1-14
 */
@Configuration
@ConditionalOnMissingBean(BlackListConfigure.class)
@ConditionalOnClass(GatewayAutoConfiguration.class)
public class BlackListConfigure {
    @Bean
    public BlackListFilter blackListFilter() {
        return new BlackListFilter();
    }
}
